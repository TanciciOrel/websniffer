CREATE TABLE `matches` (
    `url` TEXT NULL,
    `regexp` TEXT NULL,
    `matches` TEXT NULL,
    `created_at` DATETIME NULL
);