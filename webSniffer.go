// webSniffer
package main

import (
	"bufio"
	"database/sql"
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

const urlsFilePath = "urls.txt"

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	checkErr(err)

	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func processOneWeb(url, searchfor string) {
	fmt.Printf("URL: %s exp: %s\n", url, searchfor)
	db, err := sql.Open("sqlite3", "./webSniffer.db")
	checkErr(err)

	resp, err := http.Get(url)
	checkErr(err)

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err == nil {
		re := regexp.MustCompile(searchfor)
		matches := re.FindAllString(string(body), -1)
		matchesSer := strings.Join(matches, ";")
		fmt.Println(matchesSer)

		stmtS, err := db.Prepare("SELECT matches FROM matches WHERE url=? AND regexp=? ORDER BY created_at DESC LIMIT 1")
		checkErr(err)

		var storedMatches string
		err = stmtS.QueryRow(url, searchfor).Scan(&storedMatches)

		if storedMatches != matchesSer {
			cas := time.Now().Format(time.RFC3339)
			stmtI, err := db.Prepare("INSERT INTO matches(url, regexp, matches, created_at) values(?,?,?,?)")
			checkErr(err)
			_, err = stmtI.Exec(url, searchfor, matchesSer, cas)
			checkErr(err)
		}
	}
}

func processCycle() {
	urls, _ := readLines(urlsFilePath)

	for _, urlLine := range urls {
		r := csv.NewReader(strings.NewReader(urlLine))
		r.Comma = ';'

		for {
			record, err := r.Read()
			if err == io.EOF {
				break
			}
			// 			if err != nil {
			// 				log.Fatal(err)
			// 			}
			checkErr(err)

			processOneWeb(record[0], record[1])
		}
	}
}

func main() {
	for {
		processCycle()
		time.Sleep(time.Hour)
	}
}
